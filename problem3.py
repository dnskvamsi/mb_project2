import csv
from datetime import datetime
import matplotlib.pyplot as plt

pincode_vs_district = {
    "414001": "Ahmednagar",
    "444001": "Akola",
    "444601": "Amravati",
    "431001": "Aurangabad",
    "401101": "Thane",
    "421308": "Thane",
    "442401": "Chandrapur",
    "424001": "Dhule",
    "421201": "Thane",
    "416115": "Kolhapur",
    "413512": "Latur",
    "423203": "Nashik",
    "401107": "Thane",
    "400001": "Mumbai",
    "440001": "Nagpur",
    "431601": "Nanded",
    "422001": "Nashik",
    "431401": "Parbhani",
    "412303": "Pune",
    "411001": "Pune",
    "416416": "Sangli",
    "413001": "Solapur",
    "400601": "Thane",
    "1421002": "Thane",
    "401208": "Palghar",
    "401303": "Palghar",
}
no_of_registrations_by_district = {}
comp_data_2015 = {}


def date_to_year(str_date):
    try:
        y = datetime.strptime(str_date, "%d-%m-%Y")
        year = y.year
    except:
        try:
            y = datetime.strptime(str_date, "%Y-%m-%d")
            year = y.year
        except:
            year = "NA"
    return year


def file_reader(filename):
    with open(filename, "r", encoding="iso-8859-1") as k:
        comp_data = csv.DictReader(k)
        for data in comp_data:
            autho_cap = data["AUTHORIZED_CAPITAL"]
            year = date_to_year(data["DATE_OF_REGISTRATION"])
            company_addr = data["REGISTERED_OFFICE_ADDRESS"]
            pincode = company_addr.split(" ")[-1]
            company_cid = data["CORPORATE_IDENTIFICATION_NUMBER"]
            if year == 2015:
                comp_data_2015[company_cid] = pincode


def count_no_of_registrations(comp_data_2015):
    for cid in comp_data_2015:
        pincode = comp_data_2015[cid]
        if pincode in pincode_vs_district:
            district_name = pincode_vs_district[pincode]
            if district_name in no_of_registrations_by_district:
                no_of_registrations_by_district[district_name] += 1
            else:
                no_of_registrations_by_district[district_name] = 1


def draw_bar_plot(no_of_registrations_by_district):
    district_names = no_of_registrations_by_district.keys()
    count_of_registrations = no_of_registrations_by_district.values()
    plt.figure(figsize=(20, 8))
    plt.bar(district_names, count_of_registrations)
    plt.xticks(rotation=45)
    plt.xlabel("Districts")
    plt.ylabel("Count")
    plt.title("Company registration in the year 2015 by the district")
    plt.show()


def main():
    file_reader(r"/home/ubuntu/Downloads/Maharashtra_2015.csv")
    count_no_of_registrations(comp_data_2015)
    draw_bar_plot(no_of_registrations_by_district)


main()
