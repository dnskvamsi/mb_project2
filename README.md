# Company Master :: Maharashtra

### Histogram of Authorized Cap

Plot a histogram on the "Authorized Capital" 

### Bar Plot of company registration by year

Bar plot of the number of company registrations, vs. year.

### Company registration in the year 2015 by the district.

Bar plot districts and number of company registrations.

### Grouped Bar Plot.
Grouped Bar Plot by aggregating registration counts over...

   * Year of registration
   *  Principal Business Activity
### Instructions to run the programm

* Create a new virtual Environment for python3 
* Install the necessary packages (matplotlib)
* Download the data-set (matches.csv and umpires.csv)
* Enable the pylint to follow PEP8 guidelines
* Run the code in the terminal by using the command
  ```
  python3 <python file>
  ```