import csv
import matplotlib.pyplot as plt

intervals = {"<= 1L": 0, "1L to 10L": 0, "10L to 1Cr": 0, "1Cr to 10Cr": 0, "> 10Cr": 0}


def count_per_intervels(autho_cap):
    if autho_cap != "NA":
        if float(autho_cap.replace(",", "")) <= 10 ** 5:
            intervals["<= 1L"] += 1
        elif float(autho_cap.replace(",", "")) <= 10 ** 6:
            intervals["1L to 10L"] += 1
        elif float(autho_cap.replace(",", "")) <= 10 ** 7:
            intervals["10L to 1Cr"] += 1
        elif float(autho_cap.replace(",", "")) <= 10 ** 8:
            intervals["1Cr to 10Cr"] += 1
        else:
            intervals["> 10Cr"] += 1


def file_reader(filename):
    with open(filename, "r", encoding="iso-8859-1") as k:
        comp_data = csv.DictReader(k)
        for data in comp_data:
            autho_cap = data["AUTHORIZED_CAPITAL"]
            count_per_intervels(autho_cap)


def draw_plot(dictonary):
    plt.bar(list(dictonary.keys()), list(dictonary.values()))
    plt.ylabel("count")
    plt.show()


def main():
    file_reader(r"/home/ubuntu/Downloads/Maharashtra_2015.csv")
    draw_plot(intervals)


main()
