import csv
from datetime import datetime
import matplotlib.pyplot as plt

registrations_per_year = {}


def file_reader(filename):
    with open(filename, "r", encoding="iso-8859-1") as k:
        comp_data = csv.DictReader(k)
        for data in comp_data:
            autho_cap = data["AUTHORIZED_CAPITAL"]
            year = date_to_year(data["DATE_OF_REGISTRATION"])
            if year in registrations_per_year:
                registrations_per_year[year] += 1
            else:
                registrations_per_year[year] = 1


def date_to_year(str_date):
    try:
        y = datetime.strptime(str_date, "%d-%m-%Y")
        year = y.year
    except:
        try:
            y = datetime.strptime(str_date, "%Y-%m-%d")
            year = y.year
        except:
            year = "NA"
    return year


def draw_plot(registrations_per_year, size=None):
    if "NA" in registrations_per_year:
        del registrations_per_year["NA"]
    sorted_dict = {
        k: v
        for k, v in sorted(
            registrations_per_year.items(), key=lambda item: item[1], reverse=True
        )
    }
    if size is None:
        plt.bar(list(sorted_dict.keys()), list(sorted_dict.values()))
        plt.xticks(list(sorted_dict.keys()), rotation=90)
    else:
        plt.bar(list(sorted_dict.keys())[0:size], list(sorted_dict.values())[0:size])
        plt.xticks(list(sorted_dict.keys())[0:size], rotation=90)
    plt.show()


## draw_plot(registrations_per_year,20)import csv
from datetime import datetime
import matplotlib.pyplot as plt

registrations_per_year = {}


def file_reader(filename):
    with open(filename, "r", encoding="iso-8859-1") as k:
        comp_data = csv.DictReader(k)
        for data in comp_data:
            autho_cap = data["AUTHORIZED_CAPITAL"]
            year = date_to_year(data["DATE_OF_REGISTRATION"])
            if year in registrations_per_year:
                registrations_per_year[year] += 1
            else:
                registrations_per_year[year] = 1


def date_to_year(str_date):
    try:
        y = datetime.strptime(str_date, "%d-%m-%Y")
        year = y.year
    except:
        try:
            y = datetime.strptime(str_date, "%Y-%m-%d")
            year = y.year
        except:
            year = "NA"
    return year


def draw_plot(registrations_per_year, top_size=None):
    if "NA" in registrations_per_year:
        del registrations_per_year["NA"]
    sorted_dict = {
        k: v
        for k, v in sorted(
            registrations_per_year.items(), key=lambda item: item[1], reverse=True
        )
    }
    if top_size is None:
        plt.bar(list(sorted_dict.keys()), list(sorted_dict.values()))
        plt.xticks(list(sorted_dict.keys()), rotation=90)
    else:
        plt.bar(
            list(sorted_dict.keys())[0:top_size], list(sorted_dict.values())[0:top_size]
        )
        plt.xticks(list(sorted_dict.keys())[0:top_size], rotation=90)
    plt.title("Top 20 registrations count")
    plt.xlabel("Year")
    plt.ylabel("Count of Registrations")

    plt.show()


def main():
    file_reader(r"/home/ubuntu/Downloads/Maharashtra_2015.csv")
    draw_plot(registrations_per_year, 20)


main()
